<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

<div class="navigation">
	<div>
		<label class="mobile_menu" for="mobile_menu">
			<span>Menu</span>
		</label>
		<input id="mobile_menu" type="checkbox">
		<ul class="nav">
			<?
			$previousLevel = 0;
			foreach($arResult as $arItem):?>

				<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
					<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
				<?endif?>

				<?if ($arItem["IS_PARENT"]):?>

					<?if ($arItem["DEPTH_LEVEL"] == 1):?>
						<li class="<?if ($arItem["SELECTED"]):?>active<?else:?><?endif?>" class="dropdown1"><a href="<?=$arItem["LINK"]?>" class="<?if ($arItem["SELECTED"]):?>active<?else:?><?endif?>"><?=$arItem["TEXT"]?></a>
							<ul class="dropdown2">
					<?else:?>
						<li <?if ($arItem["SELECTED"]):?> class="active"<?endif?>><a href="<?=$arItem["LINK"]?>" class=""><?=$arItem["TEXT"]?></a>
							<ul>
					<?endif?>

				<?else:?>

					<?if ($arItem["PERMISSION"] > "D"):?>

						<?if ($arItem["DEPTH_LEVEL"] == 1):?>
							<li class="<?if ($arItem["SELECTED"]):?>active<?else:?><?endif?>"><a href="<?=$arItem["LINK"]?>" ><?=$arItem["TEXT"]?></a></li>
						<?else:?>
							<li<?if ($arItem["SELECTED"]):?> class="active"<?endif?>><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
						<?endif?>

					<?else:?>

						<?if ($arItem["DEPTH_LEVEL"] == 1):?>
							<li><a href="" class="<?if ($arItem["SELECTED"]):?>active<?else:?><?endif?>" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a></li>
						<?else:?>
							<li><a href="" class="denied" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a></li>
						<?endif?>

					<?endif?>

				<?endif?>

				<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

			<?endforeach?>

			<?if ($previousLevel > 1)://close last item tags?>
				<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
			<?endif?>
			<div class="clearfix"></div>
		</ul>
	</div>
</div>

<div class="menu-clear-left"></div>
<?endif?>